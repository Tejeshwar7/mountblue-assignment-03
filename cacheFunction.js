const cacheFunction = (cb) => {
  
  if (!cb) return () => null;

  const cache = {};

  if (!cb) return;

  const invoke = (...parameters) => {
    const key = JSON.stringify(parameters); 
    
    if (cache[key]) return cache[key];

    cache[key] = cb(...parameters);
    return cache[key];
  };

  return invoke;
};

module.exports = cacheFunction;

