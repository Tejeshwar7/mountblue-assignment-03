const limitFunctionCallCount = require("../limitFunctionCallCount");

const cb = (a, b) => a * b;

const result = limitFunctionCallCount(cb, 5);

console.log(result(2, 3)); 
console.log(result(2, 5));

