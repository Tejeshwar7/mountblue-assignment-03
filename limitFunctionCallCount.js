const limitFunctionCallCount = (cb, n) => {

  if (!n || !cb) return () => null;

  let counter = 0;

  const invoke = (...parameter) => {
    if (counter < n) {
      counter++;
      return cb(...parameter);
    }
    return null;
  };

  return invoke;
};

module.exports = limitFunctionCallCount;

