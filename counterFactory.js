const counterFactory = () => {
  

  let counter = 0;

  function increment (val = 2) {
    return (counter += val);
  }
  function decrement (val =2) {
    return (counter -= val);
  }
  return {
    increment,
    decrement
  }

  
};

module.exports = counterFactory;

